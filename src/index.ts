/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { RayshomarContract } from './rayshomar-contract';
export { RayshomarContract } from './rayshomar-contract';

export const contracts: any[] = [ RayshomarContract ];
